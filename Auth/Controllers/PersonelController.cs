﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Auth.Models;

namespace Auth.Controllers
{
    public class PersonelController : Controller
    {
        // GET: Personel
        
        public ActionResult Index()
        {
            if (User.IsInRole(RoleName.GlobalAdmin))
                return View("Index");
            
            return View("IndexGuest");
        }
        
        public ActionResult Guest()
        {
            return View("IndexGuest");
        }

        [Authorize(Roles = RoleName.GlobalAdmin)]
        //ACCESSIBLE ONLY BY ADIMIN // can pass multiple rows separated by comma
        public ActionResult New()
        {
            return View();
        }


        public ActionResult Dashboard()
        {
            return View();
        }
    }
}