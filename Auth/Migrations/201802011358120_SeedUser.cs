namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUser : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a1596a86-cd08-434e-bb9e-ace225cc07f8', N'guest@abc.com', 0, N'AHX058GDufQBSab5Bb+z+X5MTIUffpMzXPpoyBPECJRfqyooD8AA8yoSc5WnN9smjg==', N'7d44f16e-96f8-46d5-b82f-71bf6fce1cb3', NULL, 0, 0, NULL, 1, 0, N'guest@abc.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c17daaf1-0b3c-4a70-b892-f05aeba4bfca', N'admin@abc.com', 0, N'ADlWyzxWywqbe2CB4hlRZzwA/wt/s6KFFRoOwadq8Z38wz6yFY9A2pMXI+MGyE+bPQ==', N'399198cc-df4b-47df-8702-3efe917d0838', NULL, 0, 0, NULL, 1, 0, N'admin@abc.com')
            
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'53c6026d-dbd9-42b0-aa5f-1fd3bd2b545a', N'Superadmin')

                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c17daaf1-0b3c-4a70-b892-f05aeba4bfca', N'53c6026d-dbd9-42b0-aa5f-1fd3bd2b545a')

                ");
        }
        
        public override void Down()
        {
        }
    }
}
